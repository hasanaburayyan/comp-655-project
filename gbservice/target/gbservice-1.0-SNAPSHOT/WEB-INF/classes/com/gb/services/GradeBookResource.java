package com.gb.services;

import javax.ws.rs.core.Response;

public interface GradeBookResource {

	public Response getGradebooks();

	// @GET
	// @Path("{name}")
	// @Produces("text/xml;charset=utf-8")
	// public Response getGradebook(@PathParam("name") String name);

	public Response addGradebook(String name);

	public Response addSecondaryGradebook(long id);

	public Response updateSecondaryGradebook(long id);

	public Response deleteSecondaryGradebook(long id);

	public Response updateGradebook(String name);

	public Response deleteGradebook(long id);

	// student

	public Response getStudents(long id);

	public Response getStudent(long id, String name);

	public Response addStudent(long id, String name, String grade);

	public Response updateStudent(long id, String name, String grade);

	public Response deleteStudent(long id, String name);

}
