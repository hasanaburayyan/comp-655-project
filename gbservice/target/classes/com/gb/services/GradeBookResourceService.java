package com.gb.services;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Singleton;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang3.StringUtils;

import com.gb.dto.GradeBookDTO;
import com.gb.dto.GradeBookList;
import com.gb.dto.StudentDTO;

@Path("/gradebook")
@Singleton
public class GradeBookResourceService implements GradeBookResource {

	private Map<Long, GradeBookDTO> gradebookDB = new ConcurrentHashMap<Long, GradeBookDTO>();

	@GET
	@Produces("text/xml;charset=utf-8")
	@Override
	public Response getGradebooks() {
		String gbXML = gradebooksToXML(gradebookDB.values());
		return Response.ok(gbXML).build();
	}

	@PUT
	@Path("{name}")
	@Override
	public Response addGradebook(@PathParam("name") String title) {
		return insertGradebook(title);
	}

	@PUT
	@Path("secondary/{id}")
	@Override
	public Response addSecondaryGradebook(@PathParam("id") long id) {
		return insertGradebook(id);
	}

	@POST
	@Path("secondary/{id}")
	@Override
	public Response updateSecondaryGradebook(@PathParam("id") long id) {
		return insertGradebook(id);
	}

	@DELETE
	@Path("secondary/{id}")
	@Override
	public Response deleteSecondaryGradebook(@PathParam("id") long id) {
		GradeBookDTO gb = gradebookDB.get(id);

		if (gb == null || gb.isPrimary()) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		gradebookDB.remove(id);
		return Response.ok().build();
	}

	@POST
	@Path("{name}")
	@Override
	public Response updateGradebook(@PathParam("name") String title) {
		return insertGradebook(title);
	}

	@DELETE
	@Path("{id}")
	@Override
	public Response deleteGradebook(@PathParam("id") long id) {
		GradeBookDTO gb = gradebookDB.get(id);

		if (gb == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		gradebookDB.remove(id);
		return Response.ok().build();
	}

	// students

	@GET
	@Produces("text/xml;charset=utf-8")
	@Path("{id}/student")
	@Override
	public Response getStudents(@PathParam("id") long id) {
		GradeBookDTO gb = gradebookDB.get(id);

		if (gb == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		String gbXML = gradebookToXML(gb);
		return Response.ok(gbXML).build();
	}

	@GET
	@Path("{id}/student/{name}")
	@Produces("text/xml;charset=utf-8")
	@Override
	public Response getStudent(@PathParam("id") long id, @PathParam("name") String name) {
		GradeBookDTO gb;

		gb = this.gradebookDB.get(id);
		if (gb != null) {

			for (StudentDTO student : gb.getStudents()) {
				if (StringUtils.equals(student.getName(), name)) {
					String studentXML = studentToXML(student);
					return Response.ok(studentXML).build();
				}
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();

	}

	@POST
	@Path("{id}/student/{name}/grade/{letter}")
	@Override
	public Response addStudent(@PathParam("id") long id, @PathParam("name") String name,
			@PathParam("letter") String grade) {
		return insertStudent(id, name, grade);
	}

	@PUT
	@Path("{id}/student/{name}/grade/{letter}")
	@Override
	public Response updateStudent(@PathParam("id") long id, @PathParam("name") String name,
			@PathParam("letter") String grade) {
		return insertStudent(id, name, grade);
	}

	@DELETE
	@Path("{id}/student/{name}")
	@Override
	public Response deleteStudent(@PathParam("id") long id, @PathParam("name") String name) {
		GradeBookDTO gb = this.gradebookDB.get(id);

		if (gb == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		if (!gb.isPrimary()) {
			return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
		}

		StudentDTO toDelete = null;
		for (StudentDTO dto : gb.getStudents()) {
			if (StringUtils.equals(dto.getName(), name)) {
				toDelete = dto;
				break;
			}
		}

		// TODO: remove on secondary

		gb.getStudents().remove(toDelete);
		return Response.ok().build();
	}

	private static String gradebooksToXML(Collection<GradeBookDTO> gradebooks) {
		String xmlString = "";

		GradeBookList gb = new GradeBookList();
		gb.getGradeBooks().addAll(gradebooks);
		try {
			JAXBContext context = JAXBContext.newInstance(GradeBookList.class);
			Marshaller m = context.createMarshaller();

			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter sw = new StringWriter();
			m.marshal(gb, sw);
			xmlString = sw.toString();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return xmlString;
	}

	private static String gradebookToXML(GradeBookDTO gradebook) {
		String xmlString = "";

		try {
			JAXBContext context = JAXBContext.newInstance(GradeBookDTO.class);
			Marshaller m = context.createMarshaller();

			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter sw = new StringWriter();
			m.marshal(gradebook, sw);
			xmlString = sw.toString();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return xmlString;
	}

	private static String studentToXML(StudentDTO student) {
		String xmlString = "";

		try {
			JAXBContext context = JAXBContext.newInstance(StudentDTO.class);
			Marshaller m = context.createMarshaller();

			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter sw = new StringWriter();
			m.marshal(student, sw);
			xmlString = sw.toString();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return xmlString;
	}

	private boolean isValidStudent(StudentDTO student) {
		boolean valid;
		String grade;
		String name;

		List<String> values = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E", "F", "I", "W", "Z"));
		List<String> mod = new ArrayList<>(Arrays.asList("-", "+"));

		grade = student.getGrade();
		name = student.getName();
		valid = false;

		if (StringUtils.isNotBlank(name) && (StringUtils.isNotBlank((grade)) && grade.length() <= 2)) {
			String letter = grade.substring(0, 1);
			valid = grade.length() == 1 ? values.contains(StringUtils.upperCase(letter))
					: values.contains(StringUtils.upperCase(letter)) && mod.contains(grade.substring(1, 2))
							&& values.indexOf(StringUtils.upperCase((grade.substring(0, 1)))) < 4;
		}

		return valid;
	}

        // for primary gradebooks
	private Response insertGradebook(String name) {
		GradeBookDTO gb = new GradeBookDTO();

		// TODO: check if exist name in other servers
                for (GradeBookDTO db: this.gradebookDB.values()) {
                    if (StringUtils.equals(db.getTitle(), name)){
                        return Response.status(Response.Status.BAD_REQUEST).build();
                    }
                }
                
		long id = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;
		gb.setId(id);
		gb.setTitle(name);
		gb.setPrimary(true);
		gb.setStudents(new ArrayList<StudentDTO>());

		gradebookDB.put(id, gb);
		return Response.ok().build();
	}

        // for secondary gradebooks
	private Response insertGradebook(long id) {
		GradeBookDTO gb = new GradeBookDTO();

		// TODO: retrieve from other service the gradebook
                if (this.gradebookDB.get(id) != null) {
                    return Response.status(Response.Status.BAD_REQUEST).build();
                }
		gb.setId(id);
		gb.setPrimary(false);

		gradebookDB.put(id, gb);
		return Response.ok().build();
	}

	private Response insertStudent(long id, String name, String grade) {
		GradeBookDTO gb = gradebookDB.get(id);

		StudentDTO student = new StudentDTO();
		student.setName(name);
		student.setGrade(grade);

		if (!isValidStudent(student)) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		if (gb == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		if (!gb.isPrimary()) {
			return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
		}

		for (StudentDTO dto : gb.getStudents()) {
			if (StringUtils.equals(dto.getName(), name)) {
				dto.setGrade(grade);
				dto.setName(name);
				return Response.ok().build();
			}
		}

		// TODO add in secondary
		gb.getStudents().add(student);
		return Response.ok().build();
	}

}
