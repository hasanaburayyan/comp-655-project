/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comp655.registry_node;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.StreamingOutput;

/**
 *
 * @author Hasan
 */
@Path("/")
public class RegistryNodeResource {
    public static ArrayList<String> registered_nodes = new ArrayList<String>();
    
    @GET
    @Produces("application/xml")
    public StreamingOutput getNodes() {
        return new StreamingOutput() {         
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                PrintStream writer = new PrintStream(outputStream);
                writer.println("<registry-node>");
                writer.println("Hello From Registry");
                writer.println("</registry-node>");
            }      
        };
    }
    
    @GET
    @Produces("application/xml")
    @Path("all")
    public StreamingOutput getAllNodes() {
        validateHealthOfAllNodes();
        return new StreamingOutput() {         
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                PrintStream writer = new PrintStream(outputStream);
                writer.println("<registry-nodes>");
                for (String node : registered_nodes) {
                    writer.println("<node>");
                    writer.println("<node_address>" + node + "</node_address>");
                    writer.println("</node>");
                }
                writer.println("</registry-nodes>");
            }      
        };
    }
    
    
    @GET
    @Produces("application/xml")
    @Path("{node_address}")
    public String checkNode(@PathParam("node_address") String nodeAddress) throws MalformedURLException, ProtocolException, IOException {
        try {
            URL url = new URL("http://"+ nodeAddress + ":8080/gradebook/pinger");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(
            new InputStreamReader(con.getInputStream())
            );

            String inputLine;
            StringBuffer content = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }

            in.close();

            if (content == null || content.toString().isEmpty()) {
                return "<health_check id=\"" + nodeAddress + "\"> DOWN </health_check>";
            } else {
                return "<health_check id=\"" + nodeAddress + "\"> UP </health_check>";
            }
        } catch (Exception e) {
            return "<health_check id=\"" + nodeAddress + "\"> DOWN </health_check>";
        }
    }
    
    public static void validateHealthOfAllNodes() {
        ArrayList<String> updatedNodes = new ArrayList<String>();
        
        for (String node : registered_nodes) {
            if (healthCheck(node)) {
                updatedNodes.add(node);
            }
        }
        registered_nodes = updatedNodes;
    }
    
    public static boolean healthCheck(String nodeAddress) {
        try {
            URL url = new URL("http://"+ nodeAddress + ":8080/gradebook/pinger");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(
            new InputStreamReader(con.getInputStream())
            );

            String inputLine;
            StringBuffer content = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }

            in.close();
            
            if (content == null || content.toString().isEmpty()) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    
    @POST
    @Produces("application/xml")
    @Path("{node_address}")
    public StreamingOutput registerNode(@PathParam("node_address") String nodeAddress) {
        registered_nodes.add(nodeAddress);
        if (healthCheck(nodeAddress)) {
            return new StreamingOutput() {
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                PrintStream writer = new PrintStream(outputStream);
                writer.println("<register-node id=\"" + nodeAddress +"\">");
                    writer.println("<registered_status>TRUE</registered_status>");
                    writer.println("</register-node>");
            }      
            };
        } else {
            return new StreamingOutput() {
                public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                    PrintStream writer = new PrintStream(outputStream);
                    writer.println("<register-node id=\"" + nodeAddress +"\">");
                    writer.println("<registered_status> FALSE </registered_status>");
                    writer.println("<message> Node Address unreachable </message>");
                    writer.println("</register-node>");
                }
            };
        }
    }
    
    
}