package com.gb.services;

import static com.comp655.pinger.PingerResource.registeredNodes;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Singleton;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang3.StringUtils;

import com.gb.dto.GradeBookDTO;
import com.gb.dto.GradeBookList;
import com.gb.dto.StudentDTO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.bind.Unmarshaller;

@Path("/")
@Singleton
public class GradeBookResourceService implements GradeBookResource {

	private Map<Long, GradeBookDTO> gradebookDB = new ConcurrentHashMap<Long, GradeBookDTO>();
        private static ArrayList<String> registeredGradebooks = new ArrayList<String>();
        private static final String serverName = System.getenv("NODE_NAME");

        public GradeBookResourceService() {
            registerSelfWithRegistry();
        }
        
        private static void registerSelfWithRegistry() {
            try {
                URL url = new URL("http://registry:8080/registry/register/" + serverName);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.connect();
                System.out.println(con.getResponseCode());
                con.disconnect();
            } catch (MalformedURLException e ) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
	@GET
	@Produces("text/xml;charset=utf-8")
	@Override
	public Response getGradebooks() {
		String gbXML = gradebooksToXML(gradebookDB.values());
		return Response.ok(gbXML).build();
	}

        @GET
        @Path("/registry")
        @Produces("text/plain;charset=utf-8")
        public String getRegisteredNodes() throws IOException {
            updateRegisteredGradebooks();
            String nodes = "";
            for (String node: registeredGradebooks) {
                nodes += " " + node;
            }
            return nodes;
        }
        
        
	@PUT
	@Path("{name}")
	@Override
	public Response addGradebook(@PathParam("name") String title) {
            try {
                return insertGradebook(title);
            } catch (IOException ex) {
                Logger.getLogger(GradeBookResourceService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JAXBException ex) {
                Logger.getLogger(GradeBookResourceService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
	}

	@PUT
	@Path("secondary/{id}")
	@Override
	public Response addSecondaryGradebook(@PathParam("id") long id) {
            try {
                return insertGradebook(id);
            } catch (IOException ex) {
                Logger.getLogger(GradeBookResourceService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JAXBException ex) {
                Logger.getLogger(GradeBookResourceService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
	}

	@POST
	@Path("secondary/{id}")
	@Override
	public Response updateSecondaryGradebook(@PathParam("id") long id) {
            try {
                return insertGradebook(id);
            } catch (IOException ex) {
                Logger.getLogger(GradeBookResourceService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JAXBException ex) {
                Logger.getLogger(GradeBookResourceService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
	}

	@DELETE
	@Path("secondary/{id}")
	@Override
	public Response deleteSecondaryGradebook(@PathParam("id") long id) {
		GradeBookDTO gb = gradebookDB.get(id);
                System.out.println("SECONDARY DELETE CALLED!!!!!!!!");
		if (gb == null || gb.isPrimary()) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		gradebookDB.remove(id);
		return Response.ok().build();
	}

	@POST
	@Path("{name}")
	@Override
	public Response updateGradebook(@PathParam("name") String title) {
            try {
                return insertGradebook(title);
            } catch (IOException ex) {
                Logger.getLogger(GradeBookResourceService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JAXBException ex) {
                Logger.getLogger(GradeBookResourceService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
	}

	@DELETE
	@Path("{id}")
	@Override
	public Response deleteGradebook(@PathParam("id") long id) {
		GradeBookDTO gb = gradebookDB.get(id);
                
                if (gb == null) {
                    return Response.status(Response.Status.NOT_FOUND).build();
		}
                
                updateRegisteredGradebooks();
                if (gb.isPrimary()) {
                    for (String gradeBookNode: registeredGradebooks) {
                        try {
                            System.out.println("http://" + gradeBookNode + ":8080/gradebook/secondary/" + id);
                            URL url = new URL("http://" + gradeBookNode + ":8080/gradebook/secondary/" + id);
                            HttpURLConnection con = (HttpURLConnection) url.openConnection();
                            con.setRequestMethod("DELETE");
                            con.connect();
                            System.out.println(con.getResponseCode());
                            con.disconnect();
                            
                        } catch (MalformedURLException e ) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                
		gradebookDB.remove(id);
		return Response.ok().build();
	}

	// students

	@GET
	@Produces("text/xml;charset=utf-8")
	@Path("{id}/student")
	@Override
	public Response getStudents(@PathParam("id") long id) {
		GradeBookDTO gb = gradebookDB.get(id);

		if (gb == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		String gbXML = gradebookToXML(gb);
		return Response.ok(gbXML).build();
	}

	@GET
	@Path("{id}/student/{name}")
	@Produces("text/xml;charset=utf-8")
	@Override
	public Response getStudent(@PathParam("id") long id, @PathParam("name") String name) {
		GradeBookDTO gb;

		gb = this.gradebookDB.get(id);
		if (gb != null) {

			for (StudentDTO student : gb.getStudents()) {
				if (StringUtils.equals(student.getName(), name)) {
					String studentXML = studentToXML(student);
					return Response.ok(studentXML).build();
				}
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();

	}

	@POST
	@Path("{id}/student/{name}/grade/{letter}")
	@Override
	public Response addStudent(@PathParam("id") long id, @PathParam("name") String name,
			@PathParam("letter") String grade) {
            try {
                return insertStudent(id, name, grade);
            } catch (IOException ex) {
                Logger.getLogger(GradeBookResourceService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JAXBException ex) {
                Logger.getLogger(GradeBookResourceService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
	}

	@PUT
	@Path("{id}/student/{name}/grade/{letter}")
	@Override
	public Response updateStudent(@PathParam("id") long id, @PathParam("name") String name,
			@PathParam("letter") String grade) {
            try {
                return insertStudent(id, name, grade);
            } catch (IOException ex) {
                Logger.getLogger(GradeBookResourceService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JAXBException ex) {
                Logger.getLogger(GradeBookResourceService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
	}

	@DELETE
	@Path("{id}/student/{name}")
	@Override
	public Response deleteStudent(@PathParam("id") long id, @PathParam("name") String name) {
		GradeBookDTO gb = this.gradebookDB.get(id);

		if (gb == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		if (!gb.isPrimary()) {
			return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
		}

		StudentDTO toDelete = null;
		for (StudentDTO dto : gb.getStudents()) {
			if (StringUtils.equals(dto.getName(), name)) {
				toDelete = dto;
				break;
			}
		}

		// TODO: remove on secondary
                gb.getStudents().remove(toDelete);
                
                try {
                for (String gradebookNode: registeredGradebooks) {
                    URL url = new URL("http://" + gradebookNode + ":8080/gradebook");
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("GET");

                    BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream())
                    );
                    String inputLine;
                    StringBuffer content = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        content.append(inputLine);
                    }
                    in.close();
                    con.disconnect();


                    JAXBContext jaxbContext = JAXBContext.newInstance(GradeBookList.class);
                    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                    GradeBookList gradeBookList = (GradeBookList) jaxbUnmarshaller.unmarshal(new StringReader(content.toString()));

                    for (GradeBookDTO gradebook: gradeBookList.getGradeBooks()) {
                        if (gradebook.getId() == id) {
                            url = new URL("http://" + gradebookNode + ":8080/gradebook/secondary/" + id);
                            con = (HttpURLConnection) url.openConnection();
                            con.setRequestMethod("POST");
                            con.connect();
                            System.out.println(con.getResponseCode());
                            con.disconnect();
                        }
                    }
                    }   
                } catch (MalformedURLException e ) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JAXBException ex) {
                    Logger.getLogger(GradeBookResourceService.class.getName()).log(Level.SEVERE, null, ex);
                }
		
		return Response.ok().build();
	}

	private static String gradebooksToXML(Collection<GradeBookDTO> gradebooks) {
		String xmlString = "";

		GradeBookList gb = new GradeBookList();
		gb.getGradeBooks().addAll(gradebooks);
		try {
			JAXBContext context = JAXBContext.newInstance(GradeBookList.class);
			Marshaller m = context.createMarshaller();

			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter sw = new StringWriter();
			m.marshal(gb, sw);
			xmlString = sw.toString();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return xmlString;
	}
                
        private static void updateRegisteredGradebooks() {
            try {
                URL url = new URL("http://registry:8080/registry/register/all");
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream())
                );
                String inputLine;
                StringBuffer content = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }

                in.close();
                String stringToSearch = content.toString();

                Pattern p = Pattern.compile("(<node_address>gradebook_.</node_address>)");   // the pattern to search for
                Matcher m = p.matcher(stringToSearch);

                List<String> allMatches = new ArrayList<String>();

                while (m.find()) {
                    allMatches.add(m.group());
                }
                registeredGradebooks = new ArrayList<String>();
                for (String match : allMatches) {
                    registeredGradebooks.add(match.replaceAll("</?node_address>", ""));
                }
                
                registeredGradebooks.remove(serverName);
            } catch (MalformedURLException e ) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
	private static String gradebookToXML(GradeBookDTO gradebook) {
		String xmlString = "";

		try {
			JAXBContext context = JAXBContext.newInstance(GradeBookDTO.class);
			Marshaller m = context.createMarshaller();

			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter sw = new StringWriter();
			m.marshal(gradebook, sw);
			xmlString = sw.toString();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return xmlString;
	}

	private static String studentToXML(StudentDTO student) {
		String xmlString = "";

		try {
			JAXBContext context = JAXBContext.newInstance(StudentDTO.class);
			Marshaller m = context.createMarshaller();

			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter sw = new StringWriter();
			m.marshal(student, sw);
			xmlString = sw.toString();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return xmlString;
	}

	private boolean isValidStudent(StudentDTO student) {
		boolean valid;
		String grade;
		String name;

		List<String> values = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E", "F", "I", "W", "Z"));
		List<String> mod = new ArrayList<>(Arrays.asList("-", "+"));

		grade = student.getGrade();
		name = student.getName();
		valid = false;

		if (StringUtils.isNotBlank(name) && (StringUtils.isNotBlank((grade)) && grade.length() <= 2)) {
			String letter = grade.substring(0, 1);
			valid = grade.length() == 1 ? values.contains(StringUtils.upperCase(letter))
					: values.contains(StringUtils.upperCase(letter)) && mod.contains(grade.substring(1, 2))
							&& values.indexOf(StringUtils.upperCase((grade.substring(0, 1)))) < 4;
		}

		return valid;
	}

        private static List<GradeBookDTO> getRegisteredGradeBookList() throws IOException, JAXBException {
            List<GradeBookDTO> gradeBookLists = new ArrayList<GradeBookDTO>();
            
            for (String gradebookNode: registeredGradebooks) {
                    URL url = new URL("http://" + gradebookNode + ":8080/gradebook");
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("GET");
                    
                    BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream())
                    );
                    String inputLine;
                    StringBuffer content = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        content.append(inputLine);
                    }
                    in.close();
                    
                    
                    JAXBContext jaxbContext = JAXBContext.newInstance(GradeBookList.class);
                    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                    GradeBookList gb = (GradeBookList) jaxbUnmarshaller.unmarshal(new StringReader(content.toString()));
                    gradeBookLists.addAll(gb.getGradeBooks());
            }
            return gradeBookLists;
        }
        
        // for primary gradebooks
	private Response insertGradebook(String name) throws IOException, JAXBException {
		GradeBookDTO gb = new GradeBookDTO();

                // Update registered Gradebooks
                updateRegisteredGradebooks();
                // Send http request to each gradebook for its gradebooks
                
                List<GradeBookDTO> gradeBookList = getRegisteredGradeBookList();
                
                for (GradeBookDTO gradebook: gradeBookList) {
                    if (StringUtils.equalsIgnoreCase(gradebook.getTitle(), name)) {
                        return Response.status(Response.Status.CONFLICT).build();
                    }
                }
                // iterate through each gradebook to see if name passed in exists
                
                // If exists send bad request 409 conflict, gradebook already exists
                // if not then create
                for (GradeBookDTO db: this.gradebookDB.values()) {
                    if (StringUtils.equals(db.getTitle(), name)){
                        return Response.status(Response.Status.CONFLICT).build();
                    }
                }
                
		long id = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;
		gb.setId(id);
		gb.setTitle(name);
		gb.setPrimary(true);
		gb.setStudents(new ArrayList<StudentDTO>());

		gradebookDB.put(id, gb);
		return Response.ok().build();
	}

        // for secondary gradebooks
	private Response insertGradebook(long id) throws IOException, JAXBException {
		GradeBookDTO gb = null;
		System.out.println("Attempting to Insert Secondary GRADE BOOK!!!0");
                // update registered Gradebooks
                updateRegisteredGradebooks();
                
                List<GradeBookDTO> gradeBookList = getRegisteredGradeBookList();
                                
                for (GradeBookDTO gradebook : gradeBookList) {
                    if (gradebook.getId() == id) {
                        if (!gradebook.isPrimary()) {
                            return Response.status(Response.Status.CONFLICT).build();
                        }
                        System.out.print("Found match" + gradebook.toString());
                        gb = new GradeBookDTO();
                        gb.setId(gradebook.getId());
                        gb.setPrimary(false);
                        gb.setStudents(gradebook.getStudents());
                        gb.setTitle(gradebook.getTitle());
                    }
                }
                
                if (gb == null) {
                    return Response.status(Response.Status.BAD_REQUEST).build();
                }
                
//                if (this.gradebookDB.get(id) != null) {
//                    return Response.status(Response.Status.BAD_REQUEST).build();
//                }

                gradebookDB.remove(id);
		gradebookDB.put(id, gb);
		return Response.ok().build();
	}

	private Response insertStudent(long id, String name, String grade) throws IOException, JAXBException {
		GradeBookDTO gb = gradebookDB.get(id);              
                
		StudentDTO student = new StudentDTO();
		student.setName(name);
		student.setGrade(grade);

		if (!isValidStudent(student)) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		if (gb == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		if (!gb.isPrimary()) {
			return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
		}
                
                if (!gb.getStudents().contains(student)) {
                    gb.getStudents().add(student);
                } else {
                    for (StudentDTO dto : gb.getStudents()) {
			if (StringUtils.equals(dto.getName(), name)) {
				dto.setGrade(grade);
				dto.setName(name);
			}
                    }
                }
		

		// TODO add in secondary
                
                 updateRegisteredGradebooks();                
                
                if (gb.isPrimary()) {    
                    for (String gradebookNode: registeredGradebooks) {
                        URL url = new URL("http://" + gradebookNode + ":8080/gradebook");
                        HttpURLConnection con = (HttpURLConnection) url.openConnection();
                        con.setRequestMethod("GET");

                        BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream())
                        );
                        String inputLine;
                        StringBuffer content = new StringBuffer();

                        while ((inputLine = in.readLine()) != null) {
                            content.append(inputLine);
                        }
                        in.close();
                        con.disconnect();


                        JAXBContext jaxbContext = JAXBContext.newInstance(GradeBookList.class);
                        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                        GradeBookList gradeBookList = (GradeBookList) jaxbUnmarshaller.unmarshal(new StringReader(content.toString()));
                        
                        for (GradeBookDTO gradebook: gradeBookList.getGradeBooks()) {
                            if (gradebook.getId() == id) {
                                System.out.println("Hey Look Ma, No Hands!");
                                url = new URL("http://" + gradebookNode + ":8080/gradebook/secondary/" + id);
                                con = (HttpURLConnection) url.openConnection();
                                con.setRequestMethod("POST");
                                con.connect();
                                System.out.println(con.getResponseCode());
                                con.disconnect();
                            }
                        }
                    }
                }
                
                return Response.ok().build();
	}

}