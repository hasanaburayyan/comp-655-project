/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gb.dto;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author victo
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "gradebook-list")
public class GradeBookList {
    
    @XmlElement(name="gradebook")
    private List<GradeBookDTO> gradebooks = new ArrayList<GradeBookDTO>();

    public List<GradeBookDTO> getGradeBooks() {
        return gradebooks;
    }

    public void setGradeBooks(List<GradeBookDTO> gradebooks) {
        this.gradebooks = gradebooks;
    }
}