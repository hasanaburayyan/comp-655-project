/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gb.dto;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hasan
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "registry")
public class RegistryDTO {
    List<String> registeredNodes;
    
    public List<String> getRegisteredNodes() {return this.registeredNodes;}
    public void setRegisteredNodes(List<String> registeredNodes) {this.registeredNodes = registeredNodes;}
}
