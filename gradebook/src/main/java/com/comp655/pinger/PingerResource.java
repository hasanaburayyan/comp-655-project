/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comp655.pinger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.stream.StreamSource;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.UserDataHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author hasan
 */
@Path("/")
public class PingerResource {
    public static ArrayList<String> registeredNodes = new ArrayList<String>();
    
    @GET
    @Produces("application/xml")
    public StreamingOutput getNodes() {
        return new StreamingOutput() {         
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                PrintStream writer = new PrintStream(outputStream);
                writer.println("<pinger>");
                writer.println("Hi!");
                writer.println("</pinger>");
            }      
        };
    }
    
    @GET
    @Produces("application/xml")
    @Path("/stored_nodes")
    public StreamingOutput getRegisteredNodes() {
        try {
            checkRegistry();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new StreamingOutput() {         
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                PrintStream writer = new PrintStream(outputStream);
                writer.println("<known-nodes>");
                for (String node : registeredNodes) {
                    writer.println("<node_address>");
                    writer.println(node);
                    writer.println("</node_address>");
                }
                writer.println("</known-nodes>");
            }      
        };
    }
           
    private static void checkRegistry() throws MalformedURLException, ProtocolException, IOException, SAXException {
        URL url = new URL("http://registry:8080/registry/register/all");
//        URL url = new URL("http://localhost:8081/registry/register/all");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream())
        );
        String inputLine;
        StringBuffer content = new StringBuffer();
        
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        
        
        
        in.close();
                
        
        String stringToSearch = content.toString();

        Pattern p = Pattern.compile("(<node_address>gradebook_.</node_address>)");   // the pattern to search for
        Matcher m = p.matcher(stringToSearch);

        List<String> allMatches = new ArrayList<String>();

        while (m.find()) {
            allMatches.add(m.group());
        }
        registeredNodes = new ArrayList<String>();
        for (String match : allMatches) {
            registeredNodes.add(match.replaceAll("</?node_address>", ""));
        }
    }
    
}
