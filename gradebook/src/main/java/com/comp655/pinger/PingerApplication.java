/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comp655.pinger;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author hasan
 */
@ApplicationPath("/pinger")
public class PingerApplication extends Application{
    private Set<Object> singletons = new HashSet<Object>();
    
    public PingerApplication() {
        singletons.add(new PingerResource());
    }
    
    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
    
}
